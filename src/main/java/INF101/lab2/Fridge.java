package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    //int  items_in_fridge = 0;

    List<FridgeItem> items_list = new ArrayList<FridgeItem>();

    int items_in_fridge = items_list.size();

    public int totalSize(){
        return max_size;
    }


    @Override
    public int nItemsInFridge() {
        items_in_fridge = items_list.size();
        return items_in_fridge;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items_in_fridge < max_size) {
            items_list.add(item);
            items_in_fridge = items_list.size();
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items_in_fridge > 0) {
            items_list.remove(item);
            items_in_fridge = items_list.size();
        }
        else {
            throw new NoSuchElementException("no such element");
        }
        
    }

    @Override
    public void emptyFridge() {
        items_in_fridge = 0;
        items_list.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> expired_items = new ArrayList<FridgeItem>();

        for (FridgeItem item : items_list) {
            if (item.hasExpired()) {
                expired_items.add(item);
            }
        }
        items_list.removeAll(expired_items);
       
        items_in_fridge = items_list.size();
        return expired_items; 
    }
}